#include "servo.h"
#include "sys.h"
#include "delay.h"



void Set_compare(int compare);

    u8 dir=1;
    u8 led0pwmval=0;



/*
* brief: 舵机任意角度控制
* 
* 实现思路：舵机对应TIM9的PWM，挂载在APB2上，时钟频率168M，
*　预分频168-1，计数频率1M，即1us
* 0.5mm占空比为500,2.5mm占空比为2500，中间差了2000，１度对应2000／180＝11.11111.．．，
*/
void Angle_control(u8 angle)
{
    float unit = 11.11111;
    float temp = 500 + unit * angle;
    int output = (int)(temp);
    Set_compare(output);
}

void Set_compare(int compare)
{
    TIM9->CCR1 = compare;
}
